<?php


namespace SearchMessages\ResultFields;


final class Keyword
{
    private string $phrase;
    private int $value;

    public function __construct(string $phrase, int $value)
    {
        $this->phrase = $phrase;
        $this->value = $value;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}