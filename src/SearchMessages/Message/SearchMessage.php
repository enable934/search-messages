<?php

declare(strict_types=1);

namespace SearchMessages\Message;

final class SearchMessage
{
    private string $guid;
    private string $searchPhrase;

    public function __construct(string $guid, string $searchPhrase)
    {
        $this->guid = $guid;
        $this->searchPhrase = $searchPhrase;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getSearchPhrase(): string
    {
        return $this->searchPhrase;
    }
}
