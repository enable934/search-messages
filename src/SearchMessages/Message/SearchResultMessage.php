<?php

declare(strict_types=1);

namespace SearchMessages\Message;

use SearchMessages\ResultFields\Keyword;

final class SearchResultMessage
{
    private string $guid;
    private array $phrases;

    public function __construct(string $guid, array $phrases)
    {
        $this->guid = $guid;
        $this->phrases = $phrases;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @return array|Keyword[]
     */
    public function getPhrases(): array
    {
        return $this->phrases;
    }
}
